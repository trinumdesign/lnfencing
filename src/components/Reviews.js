import React from 'react';
import { ImQuotesRight } from 'react-icons/im'
import { MdEdit } from 'react-icons/md'
import { HiStar, HiOutlineStar } from 'react-icons/hi'
import '../styles/Reviews.css';

import ReviewsData from '../data/Reviews';

function Reviews() {
  const [showModal, setShowModal] = React.useState(false);
  const [stars, setStars] = React.useState(0);

  const GetStars = (stars) => {
    let s = [];

    for (let i = 0; i < stars; i++) {
      s.push(i);
    }

    return s;
  }

  return (
    <>
      <div id="reviews" className="twenyfv-height flexible full-width columns center">
        <br/>
        <h1 className="primary ft4 flexible center text-center spacer">What Our Customers Say</h1>
        <br/>
        <br/>
        <div className="flexible full-width center rows wrap">
          {
            ReviewsData.map(review => {
              return (
                <div className="review-cont popup flexible columns">
                  <ImQuotesRight className="quotes" />
                  <div className="flexible full-width center rows">
                    {
                      GetStars(review.stars).map(star => {
                        return <HiStar style={{marginLeft: '0.1em', marginRight: '0.1em', marginBottom: '0.15em', fontSize: '1.7em'}} className="primary"/>
                      })
                    }
                  </div>
                  <h3 className="primary">{review.comment}</h3>
                  <br/>
                  <h6 className="primary-dark opaque">{review.name || ""}</h6>
                </div>
              )
            })
          }
        </div>
        <br/>
        <br/>

        <h1 className="primary ft4 flexible center text-center spacer">Check out our Facebook Reviews</h1>
        <br/>
        <br/>
        <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Famanda.l.barnes.92%2Fposts%2F10215120162596086&show_text=true&width=500" width={500} height={735} style={{"border":"none", "overflow":"hidden"}} scrolling="no" frameBorder="0" allowFullScreen={true} allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        <br/>
        <br/>
        <a href="https://www.facebook.com/lncfencing/reviews/?ref=page_internal" target="_blank" className="fab flexible center popup primaryFill">
            <MdEdit className="tertiary" />
        </a>
      </div>
    </>
  )
}

export default Reviews;
