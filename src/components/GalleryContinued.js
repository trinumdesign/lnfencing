import React from 'react';
import '../styles/Gallery.css';

function GalleryContinued() {
  const shots = [
    ['https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603918/freelance/lnfencing/IMG_0664.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603917/freelance/lnfencing/IMG_0665.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603915/freelance/lnfencing/IMG_1025.jpg'],
    ['https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603914/freelance/lnfencing/IMG_2750.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603914/freelance/lnfencing/IMG_2753.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603914/freelance/lnfencing/IMG_2761_2.jpg'],
    ['https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603914/freelance/lnfencing/IMG_0666.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603914/freelance/lnfencing/IMG_3102.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603909/freelance/lnfencing/IMG_3104.jpg'],
    ['https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603908/freelance/lnfencing/IMG_3103.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603907/freelance/lnfencing/IMG_2565_2.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603907/freelance/lnfencing/IMG_2564.jpg'],
    ['https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603906/freelance/lnfencing/IMG_2565.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603905/freelance/lnfencing/IMG_2566.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603904/freelance/lnfencing/IMG_2566_2.jpg'],
    ['https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603904/freelance/lnfencing/IMG_2568.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603902/freelance/lnfencing/IMG_2570.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603902/freelance/lnfencing/IMG_5459.jpg'],
    ['https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603901/freelance/lnfencing/IMG_2573.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603901/freelance/lnfencing/IMG_5458.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603896/freelance/lnfencing/IMG_5466.jpg'],
    ['https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603899/freelance/lnfencing/IMG_5456.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603898/freelance/lnfencing/IMG_5461.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603898/freelance/lnfencing/IMG_5455.jpg'],
    ['https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603897/freelance/lnfencing/IMG_5460.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611603896/freelance/lnfencing/IMG_5465.jpg', 'https://res.cloudinary.com/lgxy/image/upload/w_500/v1611460723/freelance/lnfencing/IMG_5433.jpg']
  ];

  return (
    <>
      <br/>
      <br/>
      <h1 className="primary ft4 flexible center text-center spacer">Come Visit Us!</h1>
      <br/>
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d381795.72764390684!2d-88.37032165689705!3d41.61728779509707!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e9838ef2af9f9%3A0xa4daf62ea3b37fd5!2s8835%20Lisbon%20Rd%2C%20Morris%2C%20IL%2060450!5e0!3m2!1sen!2sus!4v1643388991887!5m2!1sen!2sus" width={600} height={600} style={{"border":"0", "width": "80vw", "marginLeft": "10vw", "borderRadius": "0.2em"}} allowFullScreen={true} loading="lazy"></iframe>
      <div id="gallery" className="twenyfv-height flexible full-width columns center">
        <br/>
        <br/>
        <h1 className="primary ft4 flexible center text-center spacer">More of Our Work</h1>
        <br/>
        <br/>
        {
          shots.map((row, i) => {
            return (
              <div className="flexible seventyfv-width center rows wrap">
                <div className="zoom shot-s">
                  <img src={row[0]} alt="shot"/>
                </div>
                <div className="zoom shot-s">
                  <img src={row[1]} alt="shot"/>
                </div>
                <div className="zoom shot-s">
                  <img src={row[2]} alt="shot"/>
                </div>
              </div>
            )
          })
        }
        <br/>
      </div>
    </>
  )
}

export default GalleryContinued;
