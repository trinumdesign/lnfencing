import React from 'react';
import '../styles/Toolbar.css'
import { NavLink } from 'react-router-dom';

function Toolbar({show}) {
  return (
    <>
      <nav className={"flexible collapses center elevate" + (show ? ' show' : '')}>
        <div className="spacer"></div>
        <NavLink to="/services" className="nav-tab hoverable keep primary bold flexible center uppercase">Services</NavLink>
        <NavLink to="/gallery" className="nav-tab hoverable primary flexible bold center uppercase">Gallery</NavLink>
        <NavLink to="/">
          <img src="https://res.cloudinary.com/lgxy/image/upload/w_256/v1611432983/freelance/lnfencing/iconion.png" alt="iconion" className="iconion"/>
        </NavLink>
        <NavLink to="/reviews" className="nav-tab hoverable primary flexible bold center uppercase">Reviews</NavLink>
        <NavLink to="/contact" className="nav-tab hoverable keep primary bold flexible center uppercase">Contact</NavLink>
        <div className="spacer"></div>
      </nav>
    </>
  )
}

export default Toolbar;
