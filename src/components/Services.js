import React from 'react';
import '../styles/Services.css';

import { GiSpikedFence, GiStakesFence, GiWoodenFence } from 'react-icons/gi'
import { FaStore } from 'react-icons/fa'

function Services() {
  return (
    <>
      <div id="services" className="twenyfv-height flexible full-width columns center">
        <br/>
        <br/>
        <h1 className="primary ft4 flexible center text-center spacer">Check Out Our Services</h1>
        <h4 className="primary flexible center text-center spacer">We take our job very serious and always aim to ensure customer satisfaction</h4>
        <br/>
        <br/>
        <div className="flexible services-c full-width rows">
          <div className="flexible spacer columns">
            <img src="https://res.cloudinary.com/lgxy/image/upload/w_900/v1611296998/freelance/lnfencing/IMG_5386.jpg" alt=""/>
            <div className="drop full-width flexible columns">
              <GiSpikedFence className="tertiary icon" />
              <br/>
              <h2 className="tertiary">Fence Installation</h2>
              <br/>
              <h4 className="tertiary opaque">We serve the community of Morris with reliable, sturdy, and durable fences that are designed to withstand the elements. Give us a call to schedule your installation service!</h4>
            </div>
          </div>
          <div className="zoom spacer">
            <img src="https://res.cloudinary.com/lgxy/image/upload/w_900/v1611297016/freelance/lnfencing/IMG_5401.jpg" alt=""/>
          </div>
        </div>
        <div className="flexible services-c alt full-width rows">
          <div className="zoom spacer">
            <img src="https://res.cloudinary.com/lgxy/image/upload/w_900/v1611297948/freelance/lnfencing/IMG_5405.jpg" alt=""/>
          </div>
          <div className="flexible spacer columns">
            <img src="https://res.cloudinary.com/lgxy/image/upload/w_900/v1611297000/freelance/lnfencing/IMG_5390.jpg" alt=""/>
            <div className="drop full-width flexible columns">
              <GiStakesFence className="tertiary icon" />
              <br/>
              <h2 className="tertiary">Fence Repair</h2>
              <br/>
              <h4 className="tertiary opaque">If part of your fence is worn out or broken, then it is important that you call our experts for fence repair services. Whether it is a simple repair or a more elaborate one, you can count on our team to help you make your fence look like new again.</h4>
            </div>
          </div>
        </div>
        <div className="flexible services-c full-width rows">
          <div className="flexible spacer columns">
            <img src="https://res.cloudinary.com/lgxy/image/upload/w_900/v1611298099/freelance/lnfencing/IMG_5406.jpg" alt=""/>
            <div className="drop full-width flexible columns">
              <GiWoodenFence className="tertiary icon" />
              <br/>
              <h2 className="tertiary">Residential Fencing</h2>
              <br/>
              <h4 className="tertiary opaque">Whether you're seeing iron, vinyl, or even chain link fencing, we can do it all. Call L & N Fencing Inc for your next residential fencing project!</h4>
            </div>
          </div>
          <div className="zoom spacer">
            <img src="https://res.cloudinary.com/lgxy/image/upload/w_900/v1611297940/freelance/lnfencing/IMG_2761.jpg" alt="pool"/>
          </div>
        </div>
        <div className="flexible services-c alt full-width rows">
          <div className="zoom spacer">
            <img src="https://res.cloudinary.com/lgxy/image/upload/w_900/v1611297018/freelance/lnfencing/IMG_5393.jpg" alt=""/>
          </div>
          <div className="flexible spacer columns">
            <img src="https://res.cloudinary.com/lgxy/image/upload/w_900/v1611298100/freelance/lnfencing/IMG_5407.jpg" alt=""/>
            <div className="drop full-width flexible columns">
              <FaStore className="tertiary icon" />
              <br/>
              <h2 className="tertiary">Commercial Fencing</h2>
              <br/>
              <h4 className="tertiary opaque">We are not limited to home fencing because we can do commercial fencing too. If you need a fence builder for your commercial space, look no further.</h4>
            </div>
          </div>
        </div>
        <br/>
        <br/>
      </div>
    </>
  )
}

export default Services;
