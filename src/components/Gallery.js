import React from 'react';
import '../styles/Gallery.css';

function Gallery() {
  const [activeTab, setActiveTab] = React.useState('wood');

  return (
    <>
      <div id="gallery" className="twenyfv-height flexible full-width columns center">
        <br/>
        <br/>
        <h1 className="primary ft4 flexible center text-center spacer">Gallery</h1>
        <div className="tabbar flexible center clickable wrap">
          <h2 onClick={() => setActiveTab('wood')} className={"tab-tab" + (activeTab == 'wood' ? ' active' : '')}>Wood</h2>
          <h2 onClick={() => setActiveTab('aluminum')} className={"tab-tab" + (activeTab == 'aluminum' ? ' active' : '')}>Aluminum</h2>
          <h2 onClick={() => setActiveTab('pvc')} className={"tab-tab" + (activeTab == 'pvc' ? ' active' : '')}>PVC</h2>
          <h2 onClick={() => setActiveTab('chain')} className={"tab-tab" + (activeTab == 'chain' ? ' active' : '')}>Chain Link</h2>
        </div>
        <br/>
        <div className="flexible seventyfv-width center rows wrap">
          <div className="flexible gallery-r-c columns">
            <div className={"gallery-r " + activeTab}></div>
            <div className={"gallery-r " + activeTab}></div>
          </div>
          <div className={"gallery-c " + activeTab}></div>
        </div>
        <br/>
        <div className="flexible seventyfv-width center rows wrap">
          <div className={"gallery-c s s_" + activeTab}></div>
          <div className="flexible gallery-r-c s columns">
            <div className={"gallery-r s_" + activeTab}></div>
            <div className={"gallery-r s_" + activeTab}></div>
          </div>
        </div>
        <br/>
        <br/>
      </div>
    </>
  )
}

export default Gallery;
