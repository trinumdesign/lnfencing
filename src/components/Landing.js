import React from 'react';

import { NavLink } from 'react-router-dom';

import '../styles/Landing.css'

function Landing() {

  React.useEffect(() => {
    let header = document.getElementById('header');

    setInterval(() => {
      let current = header.innerHTML;

      if (current === 'Wood') {
        header.innerHTML = 'Aluminum';
      } else if (current === 'Aluminum') {
        header.innerHTML = 'PVC';
      } else if (current === 'PVC') {
        header.innerHTML = 'Chain Link';
      } else {
        header.innerHTML = 'Wood';
      }
    }, 4000);
  }, [])

  return (
    <>
      <div id="landing" className="naityfv-height has-bg anim flexible full-width columns">
        <div className="shadow"></div>
        <nav className="flexible trans center bigger">
          <div className="spacer"></div>
          <NavLink to="/services" className="nav-tab keep tertiary bold flexible center uppercase">Services</NavLink>
          <NavLink to="/gallery" className="nav-tab tertiary flexible bold center uppercase">Gallery</NavLink>
          <NavLink to="/">
            <img src="https://res.cloudinary.com/lgxy/image/upload/w_256/v1611434286/freelance/lnfencing/iconion-white.png" alt="iconion" className="iconion"/>
          </NavLink>
          <NavLink to="/reviews" className="nav-tab tertiary flexible bold center uppercase">Reviews</NavLink>
          <NavLink to="/contact" className="nav-tab keep tertiary bold flexible center uppercase">Contact</NavLink>
          <div className="spacer"></div>
        </nav>
        <div className="spacer"></div>
        <h1 id="header" className="viga bold tertiary landing-header anim flexible center text-center spacer">Wood</h1>
        <div className="spacer"></div>
        <br/>
      </div>
    </>
  )
}

export default Landing;
