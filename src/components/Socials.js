import React from 'react';
import '../styles/Socials.css';

import { FaFacebookF, FaInstagram, FaMapMarkedAlt } from 'react-icons/fa'
import { AiFillMessage } from 'react-icons/ai'
import { FiMenu } from 'react-icons/fi'
import { HiLocationMarker } from 'react-icons/hi'
import { IoCall, IoMail } from 'react-icons/io5'
import { NavLink } from 'react-router-dom';

function Socials({}) {
  return (
    <>
      <div className="socials-side flexible row">
        <div className="flexible clickable center columns content">
          <div className="ico-cont resize-hide flexible center">
            <NavLink to="/contact"></NavLink>
            <AiFillMessage className="tertiary" />
          </div>
          <div className="ico-cont flexible center">
            <a href="https://goo.gl/maps/8LcFGHLB1bpg1Mrq6"></a>
            <FaMapMarkedAlt className="tertiary" />
          </div>
          <div className="ico-cont resize-show flexible center">
            <a href="sms:8158239185"></a>
            <AiFillMessage className="tertiary" />
          </div>
          <div className="ico-cont flexible center">
            <a target="_blank" href="tel:8158239185"></a>
            <IoCall className="tertiary" />
          </div>
          <div className="ico-cont flexible center">
            <a target="_blank" href="mailto:contact@lnfencing.com"></a>
            <IoMail className="tertiary" />
          </div>
          <div className="ico-cont flexible center">
            <a target="_blank" href="https://www.facebook.com/L-N-Fencing-104598024976337"></a>
            <FaFacebookF className="tertiary" />
          </div>
        </div>
      </div>
    </>
  )
}

export default Socials;
