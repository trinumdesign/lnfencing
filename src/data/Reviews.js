/*

  {
    name: "",
    comment: ""
  },

*/

export default [
  {
    name: "",
    comment: "this team repaired my iron fence beautifully!",
    stars: 5
  },
  {
    name: "",
    comment: "They did an amazingly job doing my backyard fence I highly recommend this company. They have great skills and customer service and you will not regret this decision.",
    stars: 5
  },
  {
    name: "",
    comment: "They came out fast and done a great job. I would recommend to anyone!",
    stars: 5
  },
  {
    name: "",
    comment: "Definitely will recommend! Very professional and job was finished the same day!",
    stars: 5
  },
  {
    name: "",
    comment: "They did an amazing job with our property. We moved and needed some help fast since we have several dogs and livestock. They ordered materials and started within two weeks. They fenced in 2 acres on our property, in the freezing cold and snow, in 5 days. It was incredible. Quality work by some hard working individuals. Thank you so much! Looks great!",
    stars: 5
  },
  {
    name: "",
    comment: "Edgar, the owner, gave me professional and courteous service. He gave me the estimate and answered all my questions. He recommended a white vinyl fence and I’m glad I took his advice. He completed the work expeditiously and left my property debris free. I’ve received many compliments from my neighbors on how the fence has added curb appeal to my home. And in addition, the cost of the fence was 15% lower than the other estimates I received. I highly recommend LN Fencing!!!",
    stars: 5
  }
]
