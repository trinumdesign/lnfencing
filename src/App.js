import React from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

import Toolbar from './components/Toolbar';
import Landing from './components/Landing';
import Gallery from './components/Gallery';
import Services from './components/Services';
import Reviews from './components/Reviews';
import GalleryContinued from './components/GalleryContinued';
import Socials from './components/Socials';
import Footer from './components/Footer';

class App extends React.Component {
  state = {
    showToolbar: false,
    showModal: false
  };

  constructor(props) {
    super(props);
    this.state = {
      showToolbar: false,
      showModal: false
    };
    props.history.listen((data) => {
      setTimeout(() => {
        this.onRouteChange(data, props.history);
      }, 500);
    });
  }

  onRouteChange(data, history) {
    if (history.action === "REPLACE") return;
    let element;

    switch(data.pathname) {
      case '/':
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
        this.setState({
          showModal: false
        });
        break;
      case '/services':
        element = document.getElementById('services');
        this.setState({
          showModal: false
        });
        break;
      case '/gallery':
        element = document.getElementById('gallery');
        this.setState({
          showModal: false
        });
        break;
      case '/reviews':
        element = document.getElementById('reviews');
        this.setState({
          showModal: false
        });
        break;
      case '/contact':
        element = document.getElementById('contact')
        this.setState({
          showModal: true
        });
        break;
      default:
        this.setState({
          showModal: false,
          location: data
        });
        break;
    }

    if (!element) return;
    element.scrollIntoView({behavior: 'smooth'});
    setTimeout(() => {
      window.scrollBy(0, -20);
    }, 1000)
  }

  componentDidMount() {
    document.addEventListener('scroll', (e) => {
      this.setState({
        showToolbar: window.scrollY > 86
      })
    });
  }

  BackToTop() {
    this.setState({
      showToolbar: false
    })
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <React.Fragment>
        <div className={"shadow " + (this.state.showModal ? 'show' : '')}></div>
        <div className={"modal flexible center columns " + (this.state.showModal ? 'show' : '')}>
          <h2 className="header primary">Contact Us</h2>
          <h4 className="description secondary text-center">Send us your questions or messages</h4>
          <div className="divider"></div>
          <form className="flexible columns full-width center" action="https://formspree.io/f/mzbkwrgw" method="POST">
            <input type="text" name="name" placeholder="Name"/>
            <input type="email" name="email" placeholder="E-Mail"/>
            <input type="tel" name="phoneNumber" placeholder="Phone Number"/>
            <input type="text" name="message" placeholder="Message" className="area"/>
            <br/>
            <div className="flexible full-width center">
              <button type="button" className="flexible center clickable hoverable trans primary">
                <NavLink to="/" className="uriabs"></NavLink>
                Cancel
              </button>
              <button type="submit" className="flexible center clickable hoverable">Submit</button>
            </div>
          </form>
        </div>
        <Toolbar show={this.state.showToolbar} />
        <Landing />
        <Services />
        <Gallery />
        <Reviews />
        <GalleryContinued />
        <Socials trigger={this.onShowModal} />
        <Footer />
      </React.Fragment>
    )
  }
}

export default withRouter(App);
